package org.login.loginsecurity.service;

import org.login.loginsecurity.model.User;

import java.util.Arrays;
import java.util.HashSet;

import org.login.loginsecurity.model.Role;
import org.login.loginsecurity.repository.RoleRepository;
import org.login.loginsecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*injecting UserRepository & RoleRepository & BCryptPasswordEndocer into UserService*/

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public User findUserByEmail(String email) {
		return userRepository.findbyEmail(email);
	}

	@Override
	@Transactional
	public void saveUser(User user) {

		
		//1. Encoding the Password 
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		//2. Setting the User=Active 
		user.setActive(1);
		//3. If the User is ADMIN, set the privileges to ADMIN
		Role userRole = roleRepository.findbyRole("ADMIN");
		//4. If not ADMIN, set the Role as Simple User
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		//5. Saving the User to UserRepository
		userRepository.save(user);
	}

}
