package org.login.loginsecurity.service;

import org.login.loginsecurity.model.User;

public interface UserService {

	public User findUserByEmail(String email);
	public void saveUser(User user);
}
