package org.login.loginsecurity.model;

import java.util.Set;
import javax.persistence.*;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name = "user_id")
	private int id;
	
	@Column (name = "email")
	@Email (message = "*Please provide a valid email address. ex.: johndoe@example.com")
	@NotEmpty (message = "*Email is required")
	private String email;
	
	@Column (name = "password")
	@Length (min = 5, max = 12, message = "*Your password must be between 5-12 characters")
	@NotEmpty (message = "*Password is required")
	@Transient
	private String password;
	
	@Column (name = "name")
	@NotEmpty (message = "*Firstname is required")
	private String name;
	
	@Column (name = "last_name")
	@NotEmpty (message = "*Lastname is required")
	private String lastName;
	
	@Column (name = "active")
	private int active;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Role> roles;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	

}
